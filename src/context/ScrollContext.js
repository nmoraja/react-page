import { createContext, useState, useEffect } from 'react';

const isWindowAvailable = typeof window !== 'undefined';

const getPageYOffset = () =>
  isWindowAvailable ? window.pageYOffset : undefined;
const getInnerHeight = () =>
  isWindowAvailable ? window.innerHeight : undefined;

const scrollContext = createContext({});

export const ScrollProvider = ({ children }) => {
  const [clientWindow, setClientWindow] = useState({
    pageYOffset: undefined,
    innerHeight: undefined,
  });

  const handleScroll = () => {
    setClientWindow({
      pageYOffset: getPageYOffset(),
      innerHeight: getInnerHeight(),
    });
  };

  useEffect(() => {
    window.addEventListener('scroll', handleScroll);
    return () => window.removeEventListener('scroll', handleScroll);
  }, []);

  return (
    <scrollContext.Provider
      value={{
        clientWindow,
      }}
    >
      {children}
    </scrollContext.Provider>
  );
};

export default scrollContext;
