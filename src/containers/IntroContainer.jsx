import useMarkdownYamlParser from '../hooks/useMarkdownYamlParser';
import Intro from '../components/Sections/Intro/Intro';

function IntroContainer() {
    const {data, loading, error} = useMarkdownYamlParser('components/Sections/Intro/data.md');
    
    return (
        <Intro data={data} error={error} loading={loading} />
    )
}

export default IntroContainer
