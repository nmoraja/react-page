import useMarkdownYamlParser from '../hooks/useMarkdownYamlParser';
import About from '../components/Sections/About/About';

function AboutContainer() {
    const {data, loading, error} = useMarkdownYamlParser('components/Sections/About/data.md');
    
    return (
        <About data={data} error={error} loading={loading} />
    )
}

export default AboutContainer
