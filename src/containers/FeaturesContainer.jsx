import useMarkdownYamlParser from '../hooks/useMarkdownYamlParser';
import Features from '../components/Sections/Features/Features';

function FeaturesContainer() {
    const {data, loading, error} = useMarkdownYamlParser('components/Sections/Features/data.md');
    
    return (
        <Features data={data} error={error} loading={loading} />
    )
}

export default FeaturesContainer
