import useMarkdownYamlParser from '../hooks/useMarkdownYamlParser';
import Services from '../components/Sections/Services/Services';

function ServicesContainer() {
    const {data, loading, error} = useMarkdownYamlParser('components/Sections/Services/data.md');
    
    return (
        <Services data={data} error={error} loading={loading} />
    )
}

export default ServicesContainer
