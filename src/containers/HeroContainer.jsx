import useMarkdownYamlParser from '../hooks/useMarkdownYamlParser';
import Hero from '../components/Sections/Hero/Hero';

function HeroContainer() {
    const {data, loading, error} = useMarkdownYamlParser('components/Sections/Hero/data.md');

    return (
        <Hero data={data} error={error} loading={loading} />
    )
}

export default HeroContainer
