import { Route, Switch } from "react-router";
import { ViewportProvider } from './context/ViewportContext';
import { ScrollProvider } from './context/ScrollContext';
import Home from "./pages/Home";
import About from "./pages/About";
import Privacy from "./pages/Privacy";
import Typography from "./pages/Typography";
import Missing from "./pages/Missing";
import Layout from "./components/Layout/Layout";


function App() {
  return (
    <ViewportProvider>
      <ScrollProvider>
        <Layout>
          <Switch>
            <Route path="/" exact component={Home} />
            <Route path="/about" component={About} />
            <Route path="/privacy" component={Privacy} />
            <Route path="/typography" component={Typography} />
            <Route path="*" component={Missing} />
          </Switch>
        </Layout>
      </ScrollProvider>
    </ViewportProvider>
  );
}

export default App;
