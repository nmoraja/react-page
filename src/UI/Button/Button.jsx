import React from 'react';
import { useHistory } from 'react-router-dom';

import classes from './Button.module.scss';

const Button = ({ type, children, onClick, to }) => {
  const history = useHistory();
  return (
    <button
      onClick={() => {
        onClick && onClick();
        history.push(to);
      }}
      className={`${classes.btn} ${type ? classes[type] : ''}`}
    >
      {children}
    </button>
  );
};

export default Button;
