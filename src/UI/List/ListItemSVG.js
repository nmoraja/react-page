const ListItemSVG = ({ fillColor = 'currentColor', width = 48, height = 48 }) => {
    return (

        <svg xmlns="http://www.w3.org/2000/svg" width="15.275" height="15.275" viewBox="0 0 15.275 15.275">
            <path
                d="M15.637,8a7.637,7.637,0,1,0,7.637,7.637A7.637,7.637,0,0,0,15.637,8Zm0,1.478a6.159,6.159,0,1,1-6.159,6.159,6.156,6.156,0,0,1,6.159-6.159m4.318,4.012-.694-.7a.37.37,0,0,0-.523,0l-4.353,4.318L12.544,15.25a.37.37,0,0,0-.523,0l-.7.694a.37.37,0,0,0,0,.523l2.8,2.818a.37.37,0,0,0,.523,0l5.315-5.272A.37.37,0,0,0,19.955,13.49Z"
                transform="translate(-8 -8)" fill="#fff"></path>
        </svg>
  );
};

export default ListItemSVG;