import React from 'react'
import { Link } from 'react-router-dom';

const List = ({items, className}) => {

    return (
        <ul className={className}>
            {items.map((item, index) => (
                <li key={index}>
                     {item.to ? <Link to={item.to}>{item.label}</Link> : item.label}
                </li>
            ))}
        </ul>
    )
}

export default List
