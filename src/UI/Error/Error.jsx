const Error = ({ error }) => {
    return (
        <div className="error">
            <p>Error: {error.message}</p>
        </div>
    )
}

export default Error;
