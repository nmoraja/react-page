import classNames from 'classnames';

import style from './Container.module.scss';

const Container = ({ children, type}) => {
    const classes = classNames({
        [style.container]: true,
        [style.fluid]: type === 'fluid',
        [style.fullWidth]: type === 'fullWidth',
    });

    return (
        <div className={ classes }>
            { children }
        </div>
    )
}

export default Container
