import React from 'react'
import LogoSVG from './LogoSVG';
import classes from "./Logo.module.scss";

const Logo = ({ fillColor, width, height }) => {
    return (
        <a href="/" className={classes.logo}>
            <LogoSVG fillColor={fillColor} width={width} height={height} />
            <span>LOGO</span>
        </a>
    )
}

export default Logo
