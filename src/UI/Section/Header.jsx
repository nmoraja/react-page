import React from 'react'

import classes from './Section.module.scss';

const Header = ( { title, subtitle, lead, children } ) => {
    return (
        <header className={classes.header}>
            <h2><span>{ subtitle }</span>{ title }</h2>
            <p className="lead">{ lead }</p>

            { children }
        </header>
    )
}

export default Header
