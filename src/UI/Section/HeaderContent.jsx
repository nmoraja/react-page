import React from 'react'

import classes from './Section.module.scss';

const HeaderContent = ( {children } ) => {
    return children ? <div className={classes.header__content}>{children}</div> : null
}

export default HeaderContent
