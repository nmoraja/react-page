import React from 'react'

import classes from './Section.module.scss';

const Content = ( { children } ) => {
    return (
        <div className={classes.content}>
            { children }
        </div>
    )
}

export default Content
