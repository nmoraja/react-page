import classes from './Section.module.scss';

const Section = ( {className, children} ) => {

    return (
        <section className={`${classes.section} ${className}`}>
            {children}
        </section>
    )
}

export default Section;
