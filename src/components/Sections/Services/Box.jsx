import Button from '../../../UI/Button/Button';
import classes from './Services.module.scss';

const Box = ({icon, alt, title, desc, button }) => {

    return (
        <div className={classes.box}>
            <div className={classes.icon}>
                <img src={`/images/icons/${icon}`} alt={alt} />
            </div>
            <div className={classes.content}>
                <h3>{title}</h3>
                <p>{desc}</p>
                <Button type='primary' to={button.to}>{button.label}</Button>
            </div>
        </div>
    )
}

export default Box
