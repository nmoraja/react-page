import Box from './Box';

import classes from './Services.module.scss';

const Boxes = ({ items }) => {
    return (
        <div className={ classes.items }>
        {
            items.map(({icon, alt, title, desc, button}, index) => 
                <Box 
                    icon={icon} 
                    alt={alt} 
                    title={title} 
                    desc={desc} 
                    button={button}
                    key={index} />
            )
        }
        </div>
    )
}

export default Boxes
