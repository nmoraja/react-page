import React from 'react';
import Error from '../../../UI/Error/Error';
import Loading from '../../../UI/Loading/Loading';
import Section from '../../../UI/Section/Section';
import Header from '../../../UI/Section/Header';
import Content from '../../../UI/Section/Content';
import Container from '../../../UI/Container/Container';
import Carousel from '../../Carousel/Carousel';

import classes from './Features.module.scss';

const Services = ({ data, error, loading }) => {
    if (error) {
        return <Error error={error} />;
    } else if (loading) {
        return <Loading />;
    }

    const { metadata: {title, subtitle, lead, items} } = data;

    return (
        <Section className={ classes.services }>
            <Container>
                <Header title={ title } subtitle={ subtitle } lead={ lead } /> 
                <Content>
                    <Carousel id="main" items={items} />
                </Content>
            </Container>
        </Section>

    )
}

export default Services
