import Box from './Box';

import classes from './Features.module.scss';

const Boxes = ({ items }) => {
    return (
        <div className={ classes.items }>
        {
            items.map(({image, alt, title, desc, button}, index) => 
                <Box 
                    image={image} 
                    alt={alt} 
                    title={title} 
                    desc={desc} 
                    button={button}
                    key={index} />
            )
        }
        </div>
    )
}

export default Boxes
