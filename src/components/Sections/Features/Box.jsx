import Button from '../../../UI/Button/Button';
import classes from './Features.module.scss';

const Box = ({image, alt, title, desc, button }) => {

    return (
        <div className={classes.box}>
            <div className={classes.image}>
                <img src={`/images/features/${image}`} alt={alt} />
            </div>
            <div className={classes.content}>
                <h3>{title}</h3>
                <p>{desc}</p>
                <Button type='primary' to={button.to}>{button.label}</Button>
            </div>
        </div>
    )
}

export default Box
