import Container from '../../../UI/Container/Container';

import classes from './Showcase.module.scss';

const Showcase = ({title , lead}) => {
    return (
        <div className={classes.showcase}>
            <Container>
                <div className={classes.wrapper}>
                    <h1 className={classes.title}>{ title }</h1>
                    <p className={`lead ${classes.lead}`}>{ lead }</p>
                </div>
            </Container>
        </div>
    )
}

export default Showcase
