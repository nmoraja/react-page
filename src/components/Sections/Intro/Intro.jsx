import Error from '../../../UI/Error/Error';
import Loading from '../../../UI/Loading/Loading';
import Container from '../../../UI/Container/Container';

import style from './Intro.module.scss';

const Intro = ({ data, error, loading }) => {
    if (error) {
        return <Error error={error} />;
    } else if (loading) {
        return <Loading />;
    }

    const {metadata, content} = data;

    return (
        <section className={style.intro}>
            <Container>
                <div className={style.wrapper}>
                    <h2>{metadata.title}</h2>
                    <p>{content}</p>
                </div>
            </Container>
        </section>
    )
}

export default Intro
