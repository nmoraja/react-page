import React from 'react';
import Markdown from 'markdown-to-jsx';
import Error from '../../../UI/Error/Error';
import Loading from '../../../UI/Loading/Loading';
import Section from '../../../UI/Section/Section';
import Header from '../../../UI/Section/Header';
import HeaderContent from '../../../UI/Section/HeaderContent';
import Content from '../../../UI/Section/Content';
import Container from '../../../UI/Container/Container';
import ItemsList from './ItemsList';

import classes from './About.module.scss';

const About = ({ data, error, loading }) => {
    if (error) {
        return <Error error={ error } />;
    } else if (loading) {
        return <Loading />;
    }

    const { content, metadata: {title, subtitle, lead, items} } = data;

    return (
        <Section className={ classes.about }>
            <Container>
                <div className={ classes.wrapper }>
                    <Header title={ title } subtitle={ subtitle } lead={ lead }> 
                        <HeaderContent>
                            <ItemsList items={ items } />
                        </HeaderContent>
                    </Header>

                    <Content>
                        <Markdown>
                            { content }
                        </Markdown>
                    </Content>
                </div>
            </Container>
        </Section>
    )
}

export default About
