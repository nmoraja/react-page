import React from 'react'
import ListItemSVG from '../../../UI/List/ListItemSVG';
import classes from './About.module.scss';

const ItemsList = ({ items }) => {
    return (
        <ul className={classes.list}>
            {items.length && items.map((item, index) => 
                <li key={index}>
                    <span>
                        <ListItemSVG />
                    </span>
                    {item}
                </li>
            )}
        </ul>
    )
}

export default ItemsList
