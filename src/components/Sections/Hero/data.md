---
image: /images/hero/hero.png
buttons:
    -
        type: primary
        to: /about
        label: Get Started
    -
        type: outline
        to: /services
        label: Services
---

# Create Amazing Landing Page With <span>CODER</span>

Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione sequinesciunt.