import Markdown from 'markdown-to-jsx';
import Error from '../../../UI/Error/Error';
import Loading from '../../../UI/Loading/Loading';
import Container from "../../../UI/Container/Container"
import Button from '../../../UI/Button/Button';

import classes from './Hero.module.scss';

const Hero = ({ data, error, loading }) => {
    if (error) {
        return <Error error={error} />;
    } else if (loading) {
        return <Loading />;
    }

    const {metadata, content} = data;

    return (
        <div className={classes.hero}>
            <Container>
                <div className={classes.wrapper}>
                    <div className={classes.start}>
                        <Markdown>
                            {content}
                        </Markdown>
                        {metadata.buttons && <ul className={classes.buttons}>
                            {metadata.buttons.map(({type, to, label}, i) => <li key={i}>
                                <Button type={type} to={to}>{label}</Button>
                            </li>)
                            }
                        </ul>
                        }
                    </div>
                    <div className={classes.end}>
                        <img className="img-fluid" src={metadata.image} alt={metadata.alt} />
                    </div>
                </div>
            </Container>
        </div>
    )
}

export default Hero
