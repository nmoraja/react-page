
const ArticleHeader = ({title, author}) => {
    return (
        <header>
            <h1>{title}</h1>
            <p>{author}</p>
        </header>
    )
}

export default ArticleHeader
