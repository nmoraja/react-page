import Markdown from 'markdown-to-jsx';
import Container from "../../UI/Container/Container";
import ArticleHeader from './ArticleHeader';

import classes from './article.module.scss';

const Content = ({ metadata, content }) => {
    const title = metadata?.title || '';
    const author =  metadata?.author || '';
    
    return (
        <Container>
            <article className={classes.article}>
                <ArticleHeader title={title} author={author} />
                <Markdown>
                    { content }
                </Markdown>
            </article>
        </Container>
    )
}

export default Content
