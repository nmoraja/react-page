import List from './../../../UI/List/List';
import classes from './Footer.module.scss';

const Menu = ({title, items}) => {
    return (
        <div className={classes.menu}>
            <h5 className={classes.title}>{title}</h5>
            <List className={classes.menu} items={items} />
        </div>
    )
}

export default Menu
