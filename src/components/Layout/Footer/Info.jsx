import Logo from '../../../UI/Logo/Logo';


import classes from './Footer.module.scss';

const Info = () => {
    return (
        <>
            <Logo fillColor={'white'} />
            <p className={`lead ${classes.lead}`}>Voluptatem eius eaque placeat incidunt 
            excepturi perferendis aut! Harum animi officia alias.</p>

        </>
    )
}

export default Info
