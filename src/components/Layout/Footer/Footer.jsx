import Info from './Info';
import Menu from './Menu';
import Container from '../../../UI/Container/Container';
import Social from '../../Social/Social';

import classes from './Footer.module.scss';

const Footer = () => {
    const items = [
        {
            to: '/',
            label: 'Home'
        },
        {
            to: '/about',
            label: 'About'
        },
        {
            to: '/privacy',
            label: 'Privacy'
        }        
    ]
    return (
        <footer className={classes.footer}>
            <Container>
                <div className={classes.wrapper}>
                    <div className={classes.start}>
                        <Info />
                        <Social className={classes.socials} />
                    </div>
                    <div className={classes.end}>
                        <Menu title={'Menu'} items={items} />
                        <Menu title={'Customer'} items={items} />
                        <Menu title={'Partners'} items={items} />
                    </div>
                </div>
            </Container>
        </footer>
    )
}

export default Footer
