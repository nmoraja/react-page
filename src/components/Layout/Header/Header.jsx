import {useContext} from 'react'
import Container from '../../../UI/Container/Container';
import Logo from '../../../UI/Logo/Logo';
import MainNavigation from './MainNavigation/MainNavigation';
import MenuToggle from './MenuToggle/MenuToggle';
import ScrollContext from '../../../context/ScrollContext';
import classes from './Header.module.scss';

const Header = () => {
    const { clientWindow } = useContext(ScrollContext);

    return (
        <header className={`${classes.header} ${clientWindow.pageYOffset ? classes.isFixed : ''}`}>
            <Container>
                <div className={classes.wrapper}>
                    <Logo />
                    <MainNavigation />
                    <MenuToggle />
                    
                </div>
            </Container>
        </header>
    )
}

export default Header
