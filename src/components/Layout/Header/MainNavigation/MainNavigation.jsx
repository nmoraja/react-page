import { NavLink } from "react-router-dom";

import style from './MainNavigation.module.scss';

const MainNavigation = () => {
    return (
        <nav>
            <ul id={ style.mainNavigation }>
                <li>
                    <NavLink exact activeClassName={ style.active } to="/about">About</NavLink>
                </li>
                <li>
                    <NavLink exact activeClassName={ style.active } to="/typography">Typography</NavLink>
                </li>
            </ul>
        </nav>
    );
}

export default MainNavigation;