import React from 'react'

import classes from './MenuToggle.module.scss';

const MenuToggle = () => {
    return (
        <div className={classes.menuToggle}>
            --- toggle ---
        </div>
    )
}

export default MenuToggle
