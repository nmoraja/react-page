import {FaFacebookF, FaTwitter, FaInstagram} from 'react-icons/fa';

const Social = ({className}) => {
    const socials = [
        {
            href: '/',
            icon: <FaFacebookF />
        },
        {
            href: '/',
            icon: <FaTwitter />
        },
        {
            href: '/',
            icon: <FaInstagram />
        }        
    ]
        
    return (
        <ul className={className}>
            {socials.map((social,  index) => (
                <li key={index}>
                    <a href={social.href}>
                        {social.icon}
                    </a>
                </li>
            ))}
        </ul>
    )
}

export default Social
