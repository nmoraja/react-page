// https://www.youtube.com/watch?v=l4kFO6VQPfA
// https://dev.to/timo_ernst/how-to-set-responsive-breakpoints-in-swiper-react-3bf6
import { Link } from 'react-router-dom';
import { Swiper, SwiperSlide } from 'swiper/react';
import SwiperCore, { Navigation, Pagination, Controller, Thumbs } from 'swiper';
import 'swiper/swiper-bundle.css';
import Button from '../../UI/Button/Button';

SwiperCore.use([Navigation, Pagination, Controller, Thumbs]);

const Carousel = ({ id, items }) => {

    const slides = [];
    for (let i = 0; i < items.length; i++) {
 
        slides.push(
          <SwiperSlide key={`slide-${i}`} tag="li">
            <div>
                <img className="img-fluid" src={`/images/features/${items[i].image}`} alt="Slide" />
                <h4>{items[i].title}</h4>
                <p>{items[i].desc}</p>
                <Link to={items[i].button.to}>
                    <Button type="primary">{items[i].button.label}</Button>
                </Link>
            </div>
          </SwiperSlide>
        );
      }

    return (
    <>
      <Swiper
        breakpoints={{
            768: {
              slidesPerView: 2,
            },
            1024: {
              slidesPerView: 3,
            },
        }}      
        id={id}
        tag="section"
        wrapperTag="ul"
        navigation
        pagination
        spaceBetween={60}
        slidesPerView={1}>
            { slides }
      </Swiper>
    </>
    )
}

export default Carousel
