import Markdown from 'markdown-to-jsx';
import Container from "../../UI/Container/Container";

import classes from './content.module.scss';

const Content = ({ content }) => {
    console.log(content);
    return (
        <Container>
            <article className={classes.article}>
                <Markdown>
                    { content }
                </Markdown>
            </article>
        </Container>
    )
}

export default Content
