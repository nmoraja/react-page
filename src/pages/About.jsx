import useMarkdownYamlParser from "../hooks/useMarkdownYamlParser";
import Error from "../UI/Error/Error";
import Loading from "../UI/Loading/Loading";
import Showcase from "../components/Sections/Showcase/Showcase"
import Article from "../components/Article/Article";

const About = () => {
    const {data, loading, error} = useMarkdownYamlParser('markdown/about.md');

    if (error) {
        return <Error error={error} />;
    } else if (loading) {
        return <Loading />;
    }    

    const { content, metadata, metadata: {showcase: {title, lead}}} = data;
    
    return (
        <>
            <Showcase title={title} lead={lead} />
            <Article metadata={metadata} content={content} />
        </>
    )
}

export default About