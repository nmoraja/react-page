import React from 'react'
import HeroContainer from '../containers/HeroContainer'
import IntroContainer from '../containers/IntroContainer'
import AboutContainer from '../containers/AboutContainer'
import ServicesContainer from '../containers/ServicesContainer'
import FeaturesContainer from '../containers/FeaturesContainer'


const Home = () => {
    return (
        <>
            <HeroContainer />
            <IntroContainer />
            <AboutContainer />
            <ServicesContainer />
            <FeaturesContainer />
        </>
    )
}

export default Home
