import React from 'react'
import useMarkdownYamlParser from "../hooks/useMarkdownYamlParser";
import Error from "../UI/Error/Error";
import Loading from "../UI/Loading/Loading";
import Showcase from "../components/Sections/Showcase/Showcase"
import Content from "../components/Content/Content";

const Privacy = () => {
    const {data, loading, error} = useMarkdownYamlParser('markdown/privacy.md');

    if (error) {
        return <Error error={error} />;
    } else if (loading) {
        return <Loading />;
    }    

    const { content, metadata: {showcase: {title, lead}}} = data;
    
    return (
        <>
            <Showcase title={title} lead={lead} />
            <Content content={content} />
        </>
    )
}

export default Privacy
