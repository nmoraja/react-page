import { useState, useEffect } from 'react';
import metadataParser from 'markdown-yaml-metadata-parser';

const useMarkdownYamlParser = source => {
  const [data, setData] = useState({});
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    import('../' + source)
      .then(res => {
        fetch(res.default)
          .then(res => res.text())
          .then(source => {
            const data = metadataParser(source);
            setData(data);
            setLoading(false);
          });
      })
      .catch(err => setError(err));
  }, [source]);

  return { data, loading, error };
};

export default useMarkdownYamlParser;
